from gan import GAN
from experience import Experience
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import imageio

""" Creates a figure and gifs displaying real sequences vs fake sequences for each task.
To run this you should have 3 folders containing the 3 tasks' experience replays
during training ("save_folder0/", "save_folder1/", "save_folder2/") and a folder
containing a GAN ("gan_save_folder/") that has been trained to generate sequences
from all of the tasks. The figure is saved as "real_fake_ims.pdf" and the gifs as
"real_ims.gif" and "fake_ims.gif". """

# load GAN
history_length = 4
observation_dims = [84, 84]
data_format = 'NCHW'
gan = GAN(tf.Session(), 100, [history_length,] + observation_dims, None, "gan_save_folder/")
gan.sess.run(tf.global_variables_initializer())
gan.restore_gen_only()

n_tasks = 3
ims_per_task = 3
real_task_ims = [[] for _ in range(n_tasks)]
fake_task_ims = [[] for _ in range(n_tasks)]

# get real sequences from experience replays
experience_length = 200000
for t in range(n_tasks):
  experience = Experience(data_format, 1, history_length, experience_length, observation_dims)
  experience.restore("save_folder"+str(t)+"/")

  for i in range(ims_per_task):
    im = experience.sample()[0][0].astype(np.uint8)
    real_task_ims[t].append(im)

def get_min_inner_len(ar):
  """ Returns the length of the smallest array in ar.

  Parameters
    ar: List of arrays.

  Returns
    Length of the smallest array.
  """
  return np.min([len(a) for a in ar])

# get fake sequences from GAN and have the user categorise them
plt.figure(1)
while get_min_inner_len(fake_task_ims) < ims_per_task:
  im = gan.gen()[0]
  plt.clf()
  plt.imshow(im[0])
  plt.pause(0.01)
  try:
    t = int(input("Enter task number 0-%d:" % (n_tasks-1)))
    if len(fake_task_ims[t]) < ims_per_task:
      fake_task_ims[t].append(im)
    print("\n", t, len(fake_task_ims[t]))
  except:
    print("error with input")

def tile(ims):
  """ Converts a 2D array of generated frames/images into a 2D array for
    displaying.

    Parameters
      raw_ims: 2D array of frames/images to convert into 2D array to display.

    Returns
      Converted array of frames/images [height, width].
  """
  c, im_h, im_w = ims[0][0].shape
  n_h, n_w = [len(ims), len(ims[0])]
  result = np.zeros((c, n_h*im_h+n_h+1, n_w*im_w+n_w+1))
  for h in range(n_h):
    for w in range(n_w):
      result[:, h*im_h+h+1:(h+1)*im_h+h+1, w*im_w+w+1:(w+1)*im_w+w+1] = ims[h][w]
  return result

# save figure
f = plt.figure(2)
ax = plt.subplot(1, 2, 1)
ax.set_adjustable('box-forced')
plt.title("Real Images")
plt.axis('off')
real_tile = tile(real_task_ims)
plt.imshow(real_tile[0], 'gray', interpolation='none', vmin=0, vmax=255)

ax2 = plt.subplot(1, 2, 2, sharex=ax, sharey=ax)
ax2.set_adjustable('box-forced')
plt.title("Generated Images")
plt.axis('off')
fake_tile = tile(fake_task_ims)
plt.imshow(fake_tile[0], 'gray', interpolation='none', vmin=0, vmax=255)

#f.gca().set_axis_off()
#plt.subplots_adjust(top = 1, bottom = 0, right = 1, left = 0, hspace = 0, wspace = 0)
#plt.margins(0,0)
#f.tight_layout()
f.gca().xaxis.set_major_locator(plt.NullLocator())
f.gca().yaxis.set_major_locator(plt.NullLocator())
plt.savefig("real_fake_ims.pdf", pad_inches=0, bbox_inches='tight')
imageio.mimsave("real_ims.gif", real_tile.astype(np.uint8))
imageio.mimsave("fake_ims.gif", fake_tile.astype(np.uint8))
plt.show()