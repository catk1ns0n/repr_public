from ac import AC
from dqn_cf import DQN_CF

class AC_CF(DQN_CF):
  """ Defines an AC agent which can use a dual memory system. This agent makes
  no effort to retain previously learnt knowledge. """
  def __init__(self, sess, env, env_test, prev_env_test, args):
    """ Initialises the AC agent.

    Parameters
      sess: Tensorflow session.
      env: Current environment to learn/play (should be a child class of Env).
      env_test: Current environment to be tested in (should be a child class of Env).
      prev_env_test: List of previously learnt environments to be tested in (should be a child
                     class of Env).
      args: Command line arguments defining the AC agent.
    """
    assert(args.load_dir is not None), "'load_dir' must be specified!"
    self.prev_env_test = prev_env_test
    AC.__init__(self, sess, env, env_test, args)