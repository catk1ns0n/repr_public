import tensorflow as tf
from logger import Logger
from session_saver import Session_Saver
import numpy as np
import sys

class Agent(Session_Saver):
  """ Defines the main functions of an agent. This class contains methods that a
  child class must override. """

  def __init__(self, sess, env, env_test, save_dir):
    """ Initialises the agent.

    Parameters
      sess: Tensorflow session.
      env: Current environment to learn/play (should be a child class of Env).
      env_test: Current environment to be tested in (should be a child class of Env).
      save_dir: Directory to save and log to.
    """
    Session_Saver.__init__(self, sess, save_dir)
    self.step = tf.Variable(0, name="step", dtype=tf.int64, trainable=False)
    self.inc_step = tf.assign_add(self.step, 1)
    self.env = env
    self.env_test = env_test

    self.log = Logger(save_dir, "agent")

    test_names = self.get_test_summary_names()
    self.test_placeholders = [tf.placeholder(tf.float32, name=name+"_placeholder") for name in test_names]
    self.test_summary = tf.summary.merge([tf.summary.scalar(name, placeholder) for name, placeholder in zip(test_names, self.test_placeholders)])

  def get_test_summary_names(self):
    """ Names of test statistics to be recorded by Tensorflow Summary.

    Returns
      List of names.
    """
    return ["av_r_test", "std_test", "min_test", "max_test"]

  def train(self, n_steps, checkpoint_hz, test_hz, restore):
    """ Train the agent on the current environment.

    Parameters
      n_steps: Number of timesteps to train until.
      checkpoint_hz: Frequency to checkpoint session.
      test_hz: Frequency to record test results.
      restore: Whether to restore from a previous training session (True) or not (False).
    """
    self.setup_train(restore)

    o = self.env.reset()
    self.setup_new_game(o)

    """
    # uncomment if for some reason you want to reinitialise the predictor and
    # target networks
    ws = sum(self.pred_network.weights, [])
    for w in ws:
      self.sess.run(w.initializer)
    self.update_target_network()
    """

    self.t = self.sess.run(self.step)
    while self.t < n_steps:
      msg = "iter: %d / %d" % (self.t, n_steps)
      sys.stdout.write(msg + chr(8) * len(msg))
      sys.stdout.flush()

      a = self.predict()

      o, r, d = self.env.step(a)
      self.observe(a, o, r, d)

      if d:
        o = self.env.reset()
        self.setup_new_game(o)

      if (self.t + 1) % test_hz == 0 or self.t == 0:
        self.record_test_results()

      self.t += 1
      self.sess.run(self.inc_step)

      if self.t % checkpoint_hz == 0:
        print("\nSaving session, do not exit!")
        sys.stdout.flush()
        self.checkpoint()
        print("saved.")
        sys.stdout.flush()

  def record_test_results(self, save_sum=True, save_vid=False):
    """ Compute and record the current agent's test results.

    Parameters
      save_sum: Whether to save the results in the Tensorflow Summary (True) or not (False).
      save_vid: Whether to save the results as a video (True) or not (False).
    """
    print("\nRecording testing results, do not exit!")
    sys.stdout.flush()

    if save_vid:
      self.env_test.start_recording(self.save_dir+"T1.mp4")
    test_results = self.play(self.env_test)
    if save_vid:
      self.env_test.end_recording()
    self.log.print("[TEST] t: %d,"%self.t, "av: %f, std: %f, min: %f, max: %f" % test_results)

    if save_sum:
      summary = self.sess.run(self.test_summary, feed_dict={x:y for x, y in zip(self.test_placeholders, test_results)})
      self.summary_writer.add_summary(summary, self.t)

    print("done.")
    sys.stdout.flush()

  def test(self):
    """ Compute and record the current agent's test results without saving them to
    the log file or Tensorflow Summary. """
    self.setup_test()
    self.log.close_log()
    self.record_test_results(False, True)

  def play(self, env, n_episodes=30):
    """ Computes the mean, std, min and max reward for the current agent in the
    environment specified.

    Parameters
      env: Environment to play (should be a child class of Env).
      n_episodes: Number of episodes to calculate the statistics from.

    Returns
      List of statistics.
    """
    ep_rewards = []

    for i in range(n_episodes):
      msg = "episode: %d / %d" % (i, n_episodes)
      sys.stdout.write(msg + chr(8) * len(msg))
      sys.stdout.flush()

      o = env.reset()
      self.setup_new_game(o, test=True)
      d = False
      total_r = 0

      while not d:
        a = self.predict(test=True)
        o, r, d = env.step(a)
        total_r += r
        self.observe(a, o, r, d, test=True)

      ep_rewards.append(total_r)

    return np.mean(ep_rewards), np.std(ep_rewards), np.min(ep_rewards), np.max(ep_rewards)

  def setup_train(self, restore):
    """ Setups the agent to be trained.

    Parameters
      restore: Whether to restore from a previous training session (True) or not (False).
    """
    self.summary_writer = tf.summary.FileWriter(self.save_dir+"summary_logs/", self.sess.graph)
    self.sess.run(tf.global_variables_initializer())

    if restore:
      print("Restoring session...")
      self.restore()
      print("done.")

  def setup_test(self):
    """ Setups the agent to be tested. """
    self.sess.run(tf.global_variables_initializer())
    self.t = -1

  def checkpoint(self):
    """ Saves the current training session. """
    Session_Saver.checkpoint(self)

  def restore(self, var_list=None):
    """ Restores a past training session.

    Parameters
      var_list: List of Tensorflow variables to restore. If None, all variables are
                restored.
    """
    Session_Saver.restore(self, var_list)

  def setup_new_game(self, o, test=False):
    """ Setups for a new game. This method is to be called after the environment
    has been reset.

    *************************************
    * This method should be overridden. *
    *************************************

    Parameters
      o: Observed state upon resetting the environment.
      test: Whether creating a new game for the testing environment (True) or the
            training environment (False).
    """
    raise NotImplementedError

  def predict(self, test=False):
    """ Predicts the best action the agent should make in the current state of
    the environment.

    *************************************
    * This method should be overridden. *
    *************************************

    Parameters
      test: Whether predicting the action for the testing environment (True) or the
            training environment (False).

    Returns
      Action.
    """
    raise NotImplementedError

  def observe(self, a, o, r, d, test=False):
    """ Method called after the agent's selected action has been applied to the
    environment. This method should update the agent accordingly.

    *************************************
    * This method should be overridden. *
    *************************************

    Parameters
      a: Action most recently taken.
      o: Observed state from taking the action.
      r: Reward received from taking the action.
      d: Whether the environment was terminal after taking the action (True) or
         not (False).
      test: Whether the action was taken in the testing environment (True) or the
            training environment (False).
    """
    raise NotImplementedError

