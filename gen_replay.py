from gan import GAN
import tensorflow as tf
import sys, os
import numpy as np
from experience import Experience
import ast

class GenReplay:
  """ Defines the replay buffer for generated items. """
  def __init__(self, genreplay_load_filename, batch_size):
    """ Initialises the replay buffer.

    Parameters
      genreplay_load_filename: File to load the replay buffer from (incl. file extension).
      batch_size: Number of examples to return from sample().
    """
    self.pseudo_ims = np.load(genreplay_load_filename)['arr_0']
    self.batch_size = batch_size

  def add(self, observation, reward, action, terminal):
    """ This method does nothing but needs to exist to be used in
    place of the experience replay. """
    pass

  def sample(self):
    """ Returns a mini-batch of randomly sampled generated items.

    Returns
      Mini-batch of generated items. Tuple: (gen_batch, None, None,
      None, None).
    """
    gen_batch = self.pseudo_ims[np.random.permutation(len(self.pseudo_ims))[:self.batch_size]]
    return gen_batch, None, None, None, None

  def checkpoint(self, prefix, f_name='exp'):
    """ This method does nothing but needs to exist to be used in
    place of the experience replay. """
    pass

  def restore(self, prefix, f_name='exp'):
    """ This method does nothing but needs to exist to be used in
    place of the experience replay. """
    pass

def main(_):
  """ Generates (or retrieves) items to fill up a replay with and then saves them for loading. """
  flag = 'exp_load_dirs'
  attr = getattr(args, flag, None)
  if attr is not None:
    setattr(args, flag, ast.literal_eval(attr))

  sess = tf.Session()
  n_items = int(2.5e5)
  get_ims_batch_size = 100
  history_length = 4
  observation_dims = [84, 84]
  in_dims = [history_length]+observation_dims

  if args.use_gan:
    gan = GAN(sess, get_ims_batch_size, in_dims, None, args.gan_load_dir, True)
    sess.run(tf.global_variables_initializer())
    gan.restore_gen_only()
  else:
    data_format = 'NCHW'
    experience_length = int(2e5)
    experiences = []
    for i in range(len(args.exp_load_dirs)):
      exp = Experience(data_format, get_ims_batch_size, history_length, experience_length, observation_dims)
      exp.restore(args.exp_load_dirs[i])
      experiences.append(exp)

  i = 0
  ims_ar = np.zeros([n_items,] + in_dims, dtype=np.uint8)
  while i < n_items:
    if args.use_gan:
      ims = gan.gen()
    else:
      ims = experiences[np.random.randint(len(experiences))].sample()[0]
    ims_ar[i:i+get_ims_batch_size] = ims
    i += get_ims_batch_size

  assert(not os.path.isfile(args.save_filename)), "Error[gen_replay]: Cannot save replay because file already exists!"
  np.savez_compressed(args.save_filename, ims_ar)

# run on GPU0
if __name__ == '__main__':
  # command line arguments
  flags = tf.app.flags
  flags.DEFINE_boolean('use_gan', True, "Whether items in the replay should be generated from a GAN (True) or retieved from experience replays (False).")
  flags.DEFINE_string('gan_load_dir', 'gan_load_folder/', "Directory to load the GAN to generate pseudo-items.")
  flags.DEFINE_string('exp_load_dirs', '["exp_load_folder/"]', "List of directories to load the experience replays to retrieve real items.")
  flags.DEFINE_string('save_filename', 'tmp_gen_replay.npz', 'File to save the replay buffer for generated/real items to (incl. file extension).')
  flags.DEFINE_integer('seed', 0, "Seed used for generating/sampling.")
  args = flags.FLAGS
  np.random.seed(args.seed)
  tf.set_random_seed(args.seed)

  os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
  os.environ["CUDA_VISIBLE_DEVICES"] = "0"
  tf.app.run()