""" Manipulated from: https://github.com/carpedm20/deep-rl-tensorflow """

import numpy as np
import gzip
import pickle

class Experience(object):
  """ Defines the experience replay memory. """

  def __init__(self, data_format, batch_size, history_length, memory_size,
    observation_dims):
    """ Initialises the replay memory.

    Parameters
      data_format: Format to return sequences in. Valid values
                   are ['NHWC', 'NCHW', None].
      batch_size: Number of examples to return from sample().
      history_length: Number of frames in a sequence.
      memory_size: Number of observations to store in memory.
      observation_dims: Dimensions of each frame in a sequence [height, width].
    """
    self.data_format = data_format
    self.batch_size = batch_size
    self.history_length = history_length
    self.memory_size = memory_size
    self.observation_dims = observation_dims

    # pre-allocate memory replay
    self.actions = np.zeros(self.memory_size, dtype=np.uint8)
    self.rewards = np.zeros(self.memory_size, dtype=np.int8)
    self.observations = np.zeros([self.memory_size] + observation_dims, dtype=np.float32 if data_format is None else np.uint8)
    self.terminals = np.zeros(self.memory_size, dtype=np.bool)

    # pre-allocate prestates and poststates for mini-batch
    self.prestates = np.zeros([self.batch_size, self.history_length] + observation_dims, dtype=np.float32 if data_format is None else np.float16)
    self.poststates = np.zeros([self.batch_size, self.history_length] + observation_dims, dtype=np.float32 if data_format is None else np.float16)

    self.count = 0
    self.current = 0

  def add(self, observation, reward, action, terminal):
    """ Adds an observation, reward, action and terminal to memory.

    Parameters
      observation: Observation to add to memory.
      reward: Reward to add to memory.
      action: Action to add to memory.
      terminal: Terminal to add to memory.
    """
    self.actions[self.current] = action
    self.rewards[self.current] = reward
    self.observations[self.current, ...] = observation
    self.terminals[self.current] = terminal
    self.count = max(self.count, self.current + 1)
    self.current = (self.current + 1) % self.memory_size

  def sample(self):
    """ Returns a mini-batch of randomly sampled experiences.
    Rewards are clipped between -1 and 1.

    Returns
      Mini-batch of experiences. Tuple: (prestates, actions, rewards,
      poststates, terminals).
    """
    assert(self.count > self.history_length), "There must be at least 'history_length + 1' items in the experience replay to call sample()."
    indexes = []
    while len(indexes) < self.batch_size:
      while True:
        # rand index between history_length and the last item in replay memory
        index = np.random.randint(self.history_length, self.count - 1)

        # make sure interval doesn't include current as this is discontinuous as
        # current is from a different episode
        if index >= self.current and index - self.history_length < self.current:
          continue
        # make sure episode doesn't terminate between interval (except on index
        # as this is our post-state (last frame))
        if self.terminals[(index - self.history_length):index].any():
          continue
        break

      self.prestates[len(indexes), ...] = self.retrieve(index - 1)
      self.poststates[len(indexes), ...] = self.retrieve(index)
      indexes.append(index)

    # set action, reward and terminal based on post-state
    actions = self.actions[indexes]
    rewards = self.rewards[indexes]
    terminals = self.terminals[indexes]

    """
    # uncomment if you want rewards to be standard normalised
    rewards = rewards.astype(np.float32)
    rewards -= np.mean(self.rewards[:self.count])
    rewards /= np.std(self.rewards[:self.count])
    """

    # move location of channel if necessary
    if self.data_format == 'NHWC' and len(self.prestates.shape) == 4:
      return np.transpose(self.prestates, (0, 2, 3, 1)), actions,\
        rewards, np.transpose(self.poststates, (0, 2, 3, 1)), terminals
    else:
      return self.prestates, actions, rewards, self.poststates, terminals

  def retrieve(self, index):
    """ Returns history_length of frames as a sequence.

    Parameters
      index: Index of the last frame to return with previous frames.

    Returns
      Sequence.
    """
    assert(index < self.count and index >= (self.history_length - 1)), "retrieve() doesn't satisfy: index < count and index >= (history_length - 1)"

    return self.observations[(index - (self.history_length - 1)):(index + 1), ...]

  def checkpoint(self, prefix, f_name='exp'):
    """ Saves the experience replay's memory.

    Parameters
      prefix: Prefix to add infront of file name when saving.
      f_name: Name of file to be saved.
    """
    pickle_vars = [self.actions, self.rewards, self.terminals, self.count, self.current]
    with gzip.open(prefix+f_name+'.pckl.gz', 'w') as f:
        pickle.dump(pickle_vars, f, protocol=pickle.HIGHEST_PROTOCOL)

    np.savez_compressed(prefix+f_name+'.npz', self.observations)

  def restore(self, prefix, f_name='exp'):
    """ Restores the experience replay's memory.

    Parameters
      prefix: Prefix to add infront of file name when restoring.
      f_name: Name of file to be restored.
    """
    with gzip.open(prefix+f_name+'.pckl.gz', 'r') as f:
        self.actions, self.rewards, self.terminals, self.count, self.current = pickle.load(f)

    self.observations = np.load(prefix+f_name+'.npz')['arr_0']

    actions_tmp = np.zeros(self.memory_size, dtype=np.uint8)
    rewards_tmp = np.zeros(self.memory_size, dtype=np.int8)
    observations_tmp = np.zeros([self.memory_size] + self.observation_dims, dtype=np.float32 if self.data_format is None else np.uint8)
    terminals_tmp = np.zeros(self.memory_size, dtype=np.bool)

    cur = self.current
    for i in range(self.count):
      actions_tmp[i] = self.actions[(cur + i) % self.count]
      rewards_tmp[i] = self.rewards[(cur + i) % self.count]
      observations_tmp[i] = self.observations[(cur + i) % self.count]
      terminals_tmp[i] = self.terminals[(cur + i) % self.count]

    self.actions = actions_tmp
    self.rewards = rewards_tmp
    self.observations = observations_tmp
    self.terminals = terminals_tmp

    i = self.memory_size - 1
    while i >= 0:
      if self.terminals[i]:
        i += 1
        break
      i -= 1
    i = np.maximum(0, i)
    self.current = i % self.memory_size
    self.count = i
