import numpy as np
import tensorflow as tf
from dqn import DQN

class AC(DQN):
  """ Defines an Actor-Critic agent which can use a dual memory system. """
  def __init__(self, sess, env, env_test, args):
    """ Initialises the Actor-Critic agent.

    Parameters
      sess: Tensorflow session.
      env: Current environment to learn/play (should be a child class of Env).
      env_test: Current environment to be tested in (should be a child class of Env).
      args: Command line arguments defining the Actor-Critic agent.
    """
    assert(args.dual_head or args.policy_only), "'dual_head' or 'policy_only' must be set to True to use the Actor-Critic."
    assert(not(args.dual_head and args.policy_only)), "'dual_head' and 'policy_only' cannot both be set to True to use the Actor-Critic."
    self.policy_B = args.policy_B
    self.entropy_B_start = args.entropy_B_start
    self.entropy_B_end = args.entropy_B_end
    self.entropy_B_t_end = args.entropy_B_t_end
    DQN.__init__(self, sess, env, env_test, args)

  def build_loss(self):
    """ Builds the loss function used by the Actor-Critic agent. """
    if self.wm:
      super().build_loss()
    else:
      with tf.variable_scope('loss'):
        self.terminals = tf.placeholder(tf.float32, [None], name="terminals")
        self.rewards = tf.placeholder(tf.float32, [None], name="rewards")
        self.n_rewards = tf.placeholder(tf.float32, [None], name="n_rewards")
        self.actions = tf.placeholder(tf.int64, [None], name="actions")
        self.pred_vs = tf.placeholder(tf.float32, [None], name="pred_vs")
        self.E_beta = tf.placeholder(tf.float32, None, name="E_beta")
        self.target_vs = tf.placeholder(tf.float32, [None], name="target_vs")

        actions_one_hot = tf.one_hot(self.actions, self.n_actions, 1.0, 0.0)

        loss_ep = 10e-6

        self.policy_loss = -self.policy_B*tf.reduce_mean(tf.log(tf.reduce_sum(self.pred_network.policy * actions_one_hot, [1]) + loss_ep)*(self.target_vs - self.pred_vs))
        self.value_loss = (tf.reduce_mean(tf.square(self.target_vs - tf.reshape(self.pred_network.value, [-1]))))
        self.entropy_loss = -self.E_beta*(-tf.reduce_mean(tf.reduce_sum(self.pred_network.policy * tf.log(self.pred_network.policy + loss_ep), [1])))

        self.t_policy_loss = self.policy_loss + self.entropy_loss
        self.t_value_loss = self.value_loss

  def build_optim(self):
    """ Builds the RMSProp optimiser used by the Actor-Critic agent. """
    if self.wm:
      super().build_optim()
    else:
      with tf.variable_scope('train_policy'):
        optimizer = tf.train.RMSPropOptimizer(self.learning_rate, decay=self.decay,
          momentum=self.momentum, epsilon=self.epsilon)

        grads_and_vars = optimizer.compute_gradients(self.t_policy_loss, self.optim_var_list)
        grads = [gv[0] for gv in grads_and_vars]
        params = [gv[1] for gv in grads_and_vars]

        grads = tf.clip_by_global_norm(grads, 1)[0]

        self.policy_optim = optimizer.apply_gradients(zip(grads, params))

      with tf.variable_scope('train_value'):
        optimizer = tf.train.RMSPropOptimizer(self.learning_rate, decay=self.decay,
          momentum=self.momentum, epsilon=self.epsilon)

        grads_and_vars = optimizer.compute_gradients(self.t_value_loss, self.optim_var_list)
        grads = [gv[0] for gv in grads_and_vars]
        params = [gv[1] for gv in grads_and_vars]

        grads = tf.clip_by_global_norm(grads, 1)[0]

        self.value_optim = optimizer.apply_gradients(zip(grads, params))

  def get_update_record_names(self):
    """ Names of statistics to be recorded by Stats.

    Returns
      List of names.
    """
    if self.wm:
      return super().get_update_record_names()
    return ['av_q', 'av_max_p', 'av_policy_loss', 'av_value_loss', 'av_entropy_loss', 'av_loss']

  def predict(self, test=False):
    """ Predicts the best action the agent should make in the current state of
    the environment.

    Parameters
      test: Whether predicting the action for the testing environment (True) or the
            training environment (False).

    Returns
      Action.
    """
    if test:
      ep = self.test_ep
      s = self.history_test.get()
    else:
      ep = np.maximum(self.ep_end, self.ep_start - np.maximum(0, self.t - self.t_learn_start)/self.t_ep_end*(self.ep_start-self.ep_end))
      s = self.history.get()

    if np.random.random() < ep:
      return np.random.randint(self.n_actions)

    state = s.reshape([1,] + self.in_dims)

    a_dist = self.sess.run(self.pred_network.policy, feed_dict={self.s: state})[0]

    return np.random.choice(range(self.n_actions), p=a_dist)

  def update(self):
    """ Updates the predictor network.

    Returns
      List containing the batch's update record values.
    """
    if self.wm:
      return super().update()
    state_batch, action_tmp, reward_batch, next_state_batch, done_batch = self.experience.sample()

    pred_vs = self.sess.run(self.pred_network.value, feed_dict={self.s: state_batch})

    target_vs = reward_batch + (1. - done_batch) * self.discount_r * np.reshape(self.sess.run(self.pred_network.value, feed_dict={self.s: next_state_batch}), [-1])

    e_beta = np.maximum(self.entropy_B_end, self.entropy_B_start - np.maximum(0, self.t - self.t_learn_start)/self.entropy_B_t_end*(self.entropy_B_start-self.entropy_B_end))

    feed_dict = {
      self.s: state_batch,
      self.terminals: done_batch,
      self.rewards: reward_batch,
      self.actions: action_tmp,
      self.pred_vs: pred_vs.flatten(),
      self.E_beta: e_beta,
      self.target_vs: target_vs
    }

    q_t, p_t, p_loss, v_loss, e_loss, _, _ = self.sess.run([self.pred_network.value, self.pred_network.policy, self.policy_loss, self.value_loss, self.entropy_loss, self.policy_optim, self.value_optim], feed_dict=feed_dict)

    return np.mean(q_t), np.mean(np.max(p_t,1)), p_loss, v_loss, e_loss, (p_loss + v_loss + e_loss)
