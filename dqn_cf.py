from dqn import DQN
import sys
import numpy as np
import tensorflow as tf

class DQN_CF(DQN):
  """ Defines a DQN agent which can use a dual memory system. This agent makes
  no effort to retain previously learnt knowledge. """
  def __init__(self, sess, env, env_test, prev_env_test, args):
    """ Initialises the DQN agent.

    Parameters
      sess: Tensorflow session.
      env: Current environment to learn/play (should be a child class of Env).
      env_test: Current environment to be tested in (should be a child class of Env).
      prev_env_test: List of previously learnt environments to be tested in (should be a child
                     class of Env).
      args: Command line arguments defining the DQN agent.
    """
    assert(args.load_dir is not None), "'load_dir' must be specified! Even though it may not be used in some cases where 'no_load' == True."
    self.prev_env_test = prev_env_test
    DQN.__init__(self, sess, env, env_test, args)

  def get_test_summary_names(self):
    """ Names of test statistics to be recorded by Tensorflow Summary.

    Returns
      List of names.
    """
    names = []
    for i in range(len(self.prev_env_test)+1):
      names.append("T%d_av_r_test"%(i+1))
      names.append("T%d_std_test"%(i+1))
      names.append("T%d_min_test"%(i+1))
      names.append("T%d_max_test"%(i+1))
    return names

  def find_best_net_from_loss(self):
    """ Returns that Stats should record the best network based upon either:
    - Dual memory system: the minimum average loss received over the current
                          recording period.
    - Standard DQN: the maximum average reward received over the current recording
                    period.

    Returns
      Boolean.
    """
    if self.wm:
      return True
    super().find_best_net_from_loss()

  def setup_train(self, restore):
    """ Setups the agent to be trained.

    Parameters
      restore: Whether to restore from a previous training session (True) or not (False).
    """
    super().setup_train(restore)

    if not restore:
      self.stats.set_best_weights()

  def record_test_results(self, save_sum=True, save_vid=False):
    """ Compute and record the current agent's test results.

    Parameters
      save_sum: Whether to save the results in the Tensorflow Summary (True) or not (False).
      save_vid: Whether to save the results as a video (True) or not (False).
    """
    print("\nRecording testing results, do not exit!")
    sys.stdout.flush()

    all_test_results = []
    for i in range(len(self.prev_env_test)):
      if save_vid:
        self.prev_env_test[i].start_recording(self.save_dir+"T%d.mp4"%(i+1))
      test_results = self.play(self.prev_env_test[i])
      if save_vid:
        self.prev_env_test[i].end_recording()
      self.log.print("[TEST-TASK%d] t: %d,"%(i+1, self.t), "av: %f, std: %f, min: %f, max: %f" % test_results)
      all_test_results.extend(test_results)

    if save_vid:
      self.env_test.start_recording(self.save_dir+"T%d.mp4"%(i+2))
    test_results = self.play(self.env_test)
    if save_vid:
      self.env_test.end_recording()
    self.log.print("[TEST-TASK%d] t: %d,"%(i+2, self.t), "av: %f, std: %f, min: %f, max: %f" % test_results)
    all_test_results.extend(test_results)

    if save_sum:
      summary = self.sess.run(self.test_summary, feed_dict={x:y for x, y in zip(self.test_placeholders, all_test_results)})
      self.summary_writer.add_summary(summary, self.t)

    print("done.")
    sys.stdout.flush()
