from env import Env
import numpy as np
import gym
import cv2
import time

class EnableRecorder:
  """ Defines methods for an environment which passes the method calls to their internal env
  instance so that it can reach the Recoder class. """

  def start_recording(self, save_path):
    """ Passes the method call to the internal env instance so that it can reach the Recoder
    class.

    Paramters
      save_path: Path to record the video at (incl. filename and extension).
    """
    self.env.start_recording(save_path)

  def end_recording(self):
    """ Passes the method call to the internal env instance so that it can reach the Recoder
    class.
    """
    self.env.end_recording()

class AtariEnv(Env, EnableRecorder):
  """ Defines an environment representing Atari 2600 games. """

  def __init__(self, env_name, observation_dims, test=False, display=False, seed=0):
    """ Initialises the environment.

    Parameters
      env_name: Name of the game ("NoFrameskip-v4" is added onto this).
      observation_dims: Dimensions that the states are resized to [height, width].
      test: Whether the environment is for testing (True) or training (False).
      display: Whether to display the environment live to the user (True) or
               not (False).
    """
    self.env = gym.make(env_name+"NoFrameskip-v4")
    self.env.seed(seed)
    self.observation_dims = observation_dims
    unwrapped_env = self.env.unwrapped
    Env.__init__(self, self.env.action_space.n, test)
    self.env = Recorder(self.env)
    if display:
      self.env = Display(self.env)
    if 'FIRE' in unwrapped_env.get_action_meanings():
      self.env = FireReset(self.env, unwrapped_env)
    self.env = NOOP(self.env, 30) # this line could also be found before fire reset
    if not self.test:
      self.env = EpisodicLife(self.env, unwrapped_env.ale)
    self.env = RepeatAction(self.env, 4)

  def step(self, a):
    """ Make an action in the environment, returning the result.

    Parameter
      a: Action.

    Returns
      List containing the observed state, reward and terminal.
    """
    if a >= self.n_actions:
      a = np.random.randint(self.n_actions)
    o, r, d, _ = self.env.step(a)
    return self.preprocess(o), r, d

  def reset(self):
    """ Reset the environment.

    Returns
      Observed state.
    """
    return self.preprocess(self.env.reset())

  def preprocess(self, raw):
    """ Preprocess an observed state to a resized grayscale image.

    Parameters
      raw: Raw observed state.

    Returns
      Preprocessed state.
    """
    result = raw.astype(np.uint8)
    result = cv2.cvtColor(result, cv2.COLOR_RGB2GRAY)
    result = cv2.resize(result, tuple(self.observation_dims), interpolation=cv2.INTER_LINEAR)
    return result

class Recorder:
  """ Defines an environment which can record its episodes, saving them as a video. """
  def __init__(self, env):
    """ Initialises the environment to be recorded.

    Parameters
      env: Environment to be recorded.
    """
    self.env = env
    self.video = None

  def start_recording(self, save_path):
    """ Begins recording the video at the specified path.

    Paramters
      save_path: Path to record the video at (incl. filename and extension).
    """
    self.video = cv2.VideoWriter(save_path, cv2.VideoWriter_fourcc(*'mp4v'), 60.0, (160, 210))

  def end_recording(self):
    """ Finishes recording the video. """
    cv2.destroyAllWindows()
    self.video.release()
    self.video = None

  def step(self, a):
    """ Make an action in the environment, returning the result and recording the observed state.

    Parameter
      a: Action.

    Returns
      List containing the observed state, reward and terminal.
    """
    o, r, d, info = self.env.step(a)
    if self.video is not None:
      self.video.write(cv2.resize(o, (160, 210), interpolation=cv2.INTER_LINEAR))
    return o, r, d, info

  def reset(self):
    """ Reset the environment.

    Returns
      Observed state.
    """
    o = self.env.reset()
    if self.video is not None:
      self.video.write(cv2.resize(o, (160, 210), interpolation=cv2.INTER_LINEAR))
    return o

  def render(self):
    """ Display the environment to the user. """
    self.env.render()

class Display(EnableRecorder):
  """ Defines an environment which displays itself live to the user. """
  def __init__(self, env):
    """ Initialises the environment to be displayed.

    Parameters
      env: Environment to be displayed.
    """
    self.env = env

  def step(self, a):
    """ Make an action in the environment, rendering the environment and returning
    the result.

    Parameter
      a: Action.

    Returns
      List containing the observed state, reward and terminal.
    """
    o, r, d, info = self.env.step(a)
    self.env.render()
    time.sleep(1/60)
    return o, r, d, info

  def reset(self):
    """ Reset and render the environment.

    Returns
      Observed state.
    """
    o = self.env.reset()
    self.env.render()
    time.sleep(1/60)
    return o

class NOOP(EnableRecorder):
  """ Defines an environment which begins with NOOP actions. """
  def __init__(self, env, max_random_start):
    """ Initialises a NOOP environment which begins with a random number of
    NOOP actions [1, max_random_start].

    Parameters
      env: Environment to make NOOP.
      max_random_start: Maximum number of NOOP actions.
    """
    self.env = env
    self.max_random_start = max_random_start

  def step(self, a):
    """ Make an action in the environment, returning the result.

    Parameter
      a: Action.

    Returns
      List containing the observed state, reward and terminal.
    """
    return self.env.step(a)

  def reset(self):
    """ Reset the environment, applying a series of NOOP actions.

    Returns
      Observed state.
    """
    o = self.env.reset()

    if self.max_random_start > 0:
      for _ in range(np.random.randint(1, self.max_random_start+1)):
        o, r, d, _ = self.step(0)
        if d:
          o = self.env.reset()

    return o

class RepeatAction(EnableRecorder):
  """ Defines an environment which repeats actions. """
  def __init__(self, env, n_action_repeat):
    """ Initialises the environment which repeats actions.

    Parameters
      env: Environment to repeat actions in.
      n_action_repeat: Number of times to repeat an action.
    """
    self.env = env
    self.n_action_repeat = n_action_repeat

  def step(self, a):
    """ Make an action multiple times in the environment, returning the result.

    Parameter
      a: Action.

    Returns
      List containing the observed state, reward and terminal.
    """
    o, r, d, info = self.env.step(a)
    if self.n_action_repeat <= 1 or d:
      return o, r, d, info

    total_r = r
    for _ in range(self.n_action_repeat-1):
      prev_o = o
      o, r, d, info = self.env.step(a)
      total_r += r
      if d:
        break

    return np.maximum(prev_o, o), total_r, d, info

  def reset(self):
    """ Reset the environment.

    Returns
      Observed state.
    """
    return self.env.reset()

class EpisodicLife(EnableRecorder):
  """ Defines an environment which is observed by the agent as terminal when a
  life is lost (although the actual environment doesn't reset until all lives
  are lost). """
  def __init__(self, env, ale):
    """ Initialises the environment which uses episodic life.

    Parameters
      env: Environment to use episodic life.
      ale: Unwrapped ALE instance of the environment.
    """
    self.env = env
    self.ale = ale
    self.lives = 0
    self.real_done = True

  def step(self, a):
    """ Make an action in the environment using episodic life, returning the result.

    Parameter
      a: Action.

    Returns
      List containing the observed state, reward and terminal.
    """
    o, r, d, info = self.env.step(a)
    self.real_done = d
    lives = self.ale.lives()
    if lives < self.lives and lives > 0:
      d = True
    self.lives = lives
    return o, r, d, info

  def reset(self):
    """ Reset the environment using episodic life.

    Returns
      Observed state.
    """
    if self.real_done:
      o = self.env.reset()
    else:
      o, _, _, _ = self.env.step(0)
    self.lives = self.ale.lives()
    return o

class FireReset(EnableRecorder):
  """ Defines an environment which begins with the fire action upon resetting
  (and when a life is lost). """
  def __init__(self, env, unwrapped_env):
    """ Initialises the environment which uses fire reset.

    Parameters
      env: Environment to use fire reset.
      unwrapped_env: Unwrapped environment.
    """
    self.env = env
    assert unwrapped_env.get_action_meanings()[1] == 'FIRE'
    assert len(unwrapped_env.get_action_meanings()) >= 3
    self.ale = unwrapped_env.ale
    self.lives = 0

  def step(self, a):
    """ Make an action in the environment, returning the result.

    Parameter
      a: Action.

    Returns
      List containing the observed state, reward and terminal.
    """
    o, r, d, info = self.env.step(a)
    lives = self.ale.lives()
    if lives < self.lives and lives > 0 and not d:
      o, r, d, info = self.env.step(1)
    self.lives = lives
    return o, r, d, info

  def reset(self):
    """ Reset the environment, beginning with the fire action.

    Returns
      Observed state.
    """
    self.env.reset()
    o, _, d, _ = self.env.step(1)
    if d:
      o = self.env.reset()
    self.lives = self.ale.lives()
    return o
