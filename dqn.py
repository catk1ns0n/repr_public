from agent import Agent
from history import History
from experience import Experience
import numpy as np
import tensorflow as tf
from stats import Stats
from models import CNN
from ops import linear

class DQN(Agent):
  """ Defines a DQN agent which can use a dual memory system. """
  def __init__(self, sess, env, env_test, args):
    """ Initialises the DQN agent.

    Parameters
      sess: Tensorflow session.
      env: Current environment to learn/play (should be a child class of Env).
      env_test: Current environment to be tested in (should be a child class of Env).
      args: Command line arguments defining the DQN agent.
    """
    Agent.__init__(self, sess, env, env_test, args.save_dir)

    self.stats_log_hz = args.stats_log_hz
    self.ep_start = args.ep_start
    self.ep_end = args.ep_end
    self.t_ep_end = args.t_ep_end
    self.test_ep = args.test_ep
    self.n_actions = 18 # env.n_actions
    self.history_length = args.history_length
    self.experience_length = args.experience_length
    self.update_hz = args.update_hz
    self.update_target_hz = args.update_target_hz
    self.batch_size = args.batch_size
    self.discount_r = args.discount_r
    self.t_learn_start = args.t_learn_start
    self.learning_rate = args.learning_rate
    self.momentum = args.momentum
    self.decay = args.decay
    self.epsilon = args.epsilon
    self.observation_dims = args.observation_dims
    self.in_dims = [self.history_length,] + self.observation_dims

    self.history = History("NCHW", self.history_length, self.observation_dims)
    self.history_test = History("NCHW", self.history_length, self.observation_dims)

    if args.teach_gen_filename is None:
      self.experience = Experience("NCHW", self.batch_size, self.history_length,
        self.experience_length, self.observation_dims)
    else:
      assert(args.wm), "'wm' must be True if 'teach_gen_filename' is not None."
      from gen_replay import GenReplay
      self.experience = GenReplay(args.teach_gen_filename, self.batch_size)

    self.filter_nums = args.filter_nums
    self.filter_shapes = args.filter_shapes
    self.filter_strides = args.filter_strides
    self.layer_order = args.layer_order
    self.hid_nums = args.hid_nums

    self.wm = args.wm
    self.policy_only = args.policy_only
    self.new_load_dir = args.new_load_dir
    self.load_dir = args.load_dir
    self.partial_load = args.partial_load
    self.no_load = args.no_load
    self.dual_head = args.dual_head
    self.dual_scale = args.dual_scale
    self.net_type = args.net_type

    if args.net_type == 'DQN':
      assert(self.wm or not self.dual_head), "When using a DQN, 'dual_head' can only be True when 'wm' is True."

    self.normQ = args.normQ
    assert(not self.normQ or (self.wm and not self.policy_only)), "If 'normQ' is True, 'wm' must be True and 'policy_only' must be False."
    if self.normQ:
      self.normQ_mean = tf.Variable(0.)
      self.normQ_std = tf.Variable(0.)

    self.build_networks()
    self.build_loss()
    self.build_optim()

    n_record_names = self.get_update_record_names()
    n_record_update = len(n_record_names) # TODO: this value could just be computed in stats
    self.stats = Stats(self.sess, self.optim_var_list, n_record_update, n_record_names,
      args.save_dir, args.stats_log_hz, min_loss=self.find_best_net_from_loss())

  def build_networks(self):
    """ Builds the networks used by the DQN agent. """
    self.s = tf.placeholder(tf.float32, [None,] + self.in_dims,
      name='s')
    self.s_ = tf.placeholder(tf.float32, [None,] + self.in_dims,
      name='s_')
    self.pred_network = self.network(self.s, "pred_network", True, dual_head=self.dual_head)
    if self.net_type == "DQN" or self.wm:
      self.target_network = self.network(self.s_, "target_network", False, dual_head=False if self.net_type == 'DQN' or self.policy_only else True)

    if not self.wm and not self.dual_head:
      self.target_network.create_copy_op(self.pred_network)

    self.optim_var_list = sum(self.pred_network.weights, [])

  def network(self, input_tensor, name, trainable, reuse=False, dual_head=False):
    """ Initialises a convolutional DQN.

    Parameters
      input_tensor: Input tensor.
      name: Network name.
      trainable: Whether the network's parameters are trainable (True) or not (False).
      reuse: Whether to reuse an existing network's weight tensors (True) or not (False).
    """
    data_format = 'NCHW'

    network = CNN(self.sess, input_tensor/255., name, None if dual_head else self.n_actions, trainable=trainable,
      data_format=data_format, filter_nums=self.filter_nums,
      filter_shapes=self.filter_shapes, filter_strides=self.filter_strides,
      layer_order=self.layer_order, hid_nums=[] if dual_head else self.hid_nums,
      weights_initializer=tf.truncated_normal_initializer(0, 1e-2),
      biases_initializer=tf.constant_initializer(1e-2), reuse=reuse, filter_padding='VALID')

    if dual_head:
      network.policy_weights = network.weights.copy()
      network.value_weights = network.weights.copy()
      with tf.variable_scope(name) as scope:
        if reuse:
          scope.reuse_variables()


        # Value
        layer_out = network.outputs

        # FC layer
        for i in range(len(self.hid_nums)):
          layer_out, Ws = linear("fc-%d"%i, layer_out, self.hid_nums[i],
            trainable=trainable, weights_initializer=tf.truncated_normal_initializer(0, 1e-2),
            biases_initializer=tf.constant_initializer(1e-2))
          print(layer_out)
          print(Ws)
          print()
          layer_out = tf.nn.relu(layer_out)
          network.weights.append(Ws)
          network.value_weights.append(Ws)

        # Linear layer
        layer_out, Ws = linear("out", layer_out, 1 if self.net_type == "AC" else self.n_actions, trainable=trainable,
          weights_initializer=tf.truncated_normal_initializer(0, 1e-2), biases_initializer=tf.constant_initializer(1e-2))
        print(layer_out)
        print(Ws)
        print()
        network.weights.append(Ws)
        network.value_weights.append(Ws)

        network.value = layer_out


        # Policy
        layer_out = network.outputs

        # FC layer
        for i in range(len(self.hid_nums)):
          layer_out, Ws = linear("fc-policy-%d"%i, layer_out, self.hid_nums[i],
            trainable=trainable, weights_initializer=tf.truncated_normal_initializer(0, 1e-2),
            biases_initializer=tf.constant_initializer(1e-2))
          print(layer_out)
          print(Ws)
          print()
          layer_out = tf.nn.relu(layer_out)
          network.weights.append(Ws)
          network.policy_weights.append(Ws)

        # Linear layer
        layer_out, Ws = linear("out-policy", layer_out, self.n_actions, trainable=trainable,
          weights_initializer=tf.truncated_normal_initializer(0, 1e-2), biases_initializer=tf.constant_initializer(1e-2))
        print(layer_out)
        print(Ws)
        print()
        network.weights.append(Ws)
        network.policy_weights.append(Ws)

        network.policy = tf.nn.softmax(layer_out)
        network.logits = layer_out

        network.outputs = None

    return network

  def build_loss(self):
    """ Builds the loss function used by the DQN agent. """
    with tf.variable_scope('loss'):
      if self.wm:
        if self.policy_only:
          self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.pred_network.outputs, labels=tf.one_hot(tf.argmax(self.target_network.outputs, 1), self.n_actions)))
        else:
          if self.dual_head:
            target_value = self.target_network.outputs if self.net_type == "DQN" else self.target_network.value
            if self.normQ:
              target_value = (target_value-self.normQ_mean)/self.normQ_std
            self.value_loss = tf.reduce_mean(tf.square(self.pred_network.value - target_value))
            #self.policy_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.pred_network.logits, labels=tf.one_hot(tf.argmax((self.target_network.outputs if self.net_type == "DQN" else self.target_network.policy), 1), self.n_actions)))
            self.policy_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self.pred_network.logits, labels=tf.one_hot(tf.argmax(self.target_network.outputs, 1), self.n_actions) if self.net_type == "DQN" else self.target_network.policy))
            self.loss = self.dual_scale*self.value_loss + (1-self.dual_scale)*self.policy_loss
          else:
            target_value = self.target_network.outputs
            if self.normQ:
              target_value = (target_value-self.normQ_mean)/self.normQ_std
            self.loss = tf.reduce_mean(tf.square(self.pred_network.outputs - target_value))
      else:
        self.terminals = tf.placeholder(tf.float32, [None], name="terminals")
        self.rewards = tf.placeholder(tf.float32, [None], name="rewards")
        self.actions = tf.placeholder(tf.int64, [None], name="actions")

        self.target_qs = self.rewards + (1. - self.terminals) * self.discount_r * tf.reduce_max(self.target_network.outputs, 1)

        actions_one_hot = tf.one_hot(self.actions, self.n_actions, 1.0, 0.0)
        self.pred_qs = tf.reduce_sum(self.pred_network.outputs * actions_one_hot, reduction_indices=1)

        self.delta = self.target_qs - self.pred_qs

        self.loss = tf.reduce_mean(tf.square(self.delta))

  def build_optim(self):
    """ Builds the RMSProp optimiser used by the DQN agent. """
    with tf.variable_scope('train'):
      optimizer = tf.train.RMSPropOptimizer(self.learning_rate, decay=self.decay,
        momentum=self.momentum, epsilon=self.epsilon)

      grads_and_vars = optimizer.compute_gradients(self.loss, self.optim_var_list)
      grads = [gv[0] for gv in grads_and_vars]
      params = [gv[1] for gv in grads_and_vars]

      grads = tf.clip_by_global_norm(grads, 1)[0]

      self.optim = optimizer.apply_gradients(zip(grads, params))

  def get_update_record_names(self):
    """ Names of statistics to be recorded by Stats.

    Returns
      List of names.
    """
    if self.dual_head:
      return ['av_q', 'av_policy_loss', 'av_value_loss', 'av_loss']
    return ['av_q', 'av_loss']

  def find_best_net_from_loss(self):
    """ Returns that Stats should record the best network based upon the maximum
    average reward received over the current recording period.

    Returns
      False.
    """
    return False

  def restore(self):
    """ Restores a past training session, including the experience replay and
    stats. """
    Agent.restore(self)
    self.experience.restore(self.save_dir)
    self.stats.restore()

  def checkpoint(self):
    """ Saves the current training session, including the experience replay and
    stats. """
    Agent.checkpoint(self)
    self.experience.checkpoint(self.save_dir)
    self.stats.checkpoint()

  def setup_new_game(self, o, test=False):
    """ Setups for a new game.

    Parameters
      o: Observed state upon resetting the environment.
      test: Whether creating a new game for the testing environment (True) or the
            training environment (False).
    """
    for _ in range(self.history_length):
      if test:
        self.history_test.add(o)
      else:
        self.history.add(o)

  def setup_train(self, restore):
    """ Setups the agent to be trained.

    Parameters
      restore: Whether to restore from a previous training session (True) or not (False).
    """
    Agent.setup_train(self, restore)

    if not restore:
      if self.load_dir is not None:
        import gzip, pickle
        ws = sum(self.pred_network.weights, [])
        with gzip.open(self.load_dir+'stats.pckl.gz', 'r') as f:
          best_weights = pickle.load(f)[0]
        if not self.no_load:
          if len(best_weights) > len(ws):
            assert(self.policy_only), "Error[dqn]: 'policy_only' must be True when the length of pred_network weights is greater than the length of the weights being loaded."
            assert(len(best_weights) == (len(ws) + 2*len(self.hid_nums)+2)), "Error[dqn]: Length of pred_network weights does not equal that of the weights being loaded (minus a second output head)."
            for i in range(len(ws)):
              j = i if i < 2*len(self.filter_nums) else i+2*len(self.hid_nums)+2
              self.sess.run(tf.assign(ws[i], best_weights[j]))
          elif len(ws) > len(best_weights):
            assert(self.partial_load), "Error[dqn]: 'partial_load' must be True when the length of pred_network weights is greater than the length of the weights being loaded."
            assert(self.net_type == "AC"), "Error[dqn]: 'net_type' must be AC when the length of pred_network weights is greater than the length of the weights being loaded."
            assert(len(best_weights) == (len(ws) - (2*len(self.hid_nums)+2))), "Error[dqn]: Length of pred_network weights (minus a second output head) does not equal that of the weights being loaded."
            for i in range(len(best_weights)):
              j = i if i < 2*len(self.filter_nums) else i+2*len(self.hid_nums)+2
              self.sess.run(tf.assign(ws[j], best_weights[i]))
          else:
            exclude = []
            if self.partial_load:
              exclude.extend([len(ws)-1, len(ws)-2])
              if self.dual_head:
                exclude.extend([2*len(self.filter_nums)+2*len(self.hid_nums), 2*len(self.filter_nums)+2*len(self.hid_nums)+1])
            for i in range(len(ws)):
              if i not in exclude:
                self.sess.run(tf.assign(ws[i], best_weights[i]))

      if self.wm:
        import gzip, pickle
        ws = sum(self.target_network.weights, [])
        with gzip.open(self.new_load_dir+'stats.pckl.gz', 'r') as f:
          best_weights = pickle.load(f)[0]
        if len(best_weights) > len(ws):
          assert(self.policy_only), "Error[dqn]: 'policy_only' must be True when the length of target_network weights is greater than the length of the weights being loaded."
          assert(len(best_weights) == (len(ws) + 2*len(self.hid_nums)+2)), "Error[dqn]: Length of target_network weights does not equal that of the weights being loaded (minus a second output head)."
          for i in range(len(ws)):
            j = i if i < 2*len(self.filter_nums) else i+2*len(self.hid_nums)+2
            self.sess.run(tf.assign(ws[i], best_weights[j]))
        else:
          for i in range(len(best_weights)):
            self.sess.run(tf.assign(ws[i], best_weights[i]))

    if self.normQ:
      new_experience = Experience("NCHW", self.batch_size, self.history_length,
        self.experience_length, self.observation_dims)
      new_experience.restore(self.new_load_dir)
      q_array = []
      for _ in range(1000):
        state = new_experience.sample()[0]
        q_array.extend(self.sess.run(self.target_network.value if self.dual_head else self.target_network.outputs, feed_dict={self.s_: state}))
      self.sess.run(tf.assign(self.normQ_mean, np.mean(q_array)))
      self.sess.run(tf.assign(self.normQ_std, np.std(q_array)))
      self.log.print("normQ_mean: %f, normQ_std: %f" % (self.sess.run(self.normQ_mean), self.sess.run(self.normQ_std)))

  def setup_test(self):
    """ Setups the agent to be tested. """
    Agent.setup_test(self)
    self.stats.restore()
    self.stats.load_best_weights()

  def predict(self, test=False):
    """ Predicts the best action the agent should make in the current state of
    the environment.

    Parameters
      test: Whether predicting the action for the testing environment (True) or the
            training environment (False).

    Returns
      Action.
    """
    if test:
      ep = self.test_ep
      s = self.history_test.get()
    else:
      ep = np.maximum(self.ep_end, self.ep_start - np.maximum(0, self.t - self.t_learn_start)/self.t_ep_end*(self.ep_start-self.ep_end))
      s = self.history.get()

    if np.random.random() < ep:
      return np.random.randint(self.n_actions)

    state = s.reshape([1,] + self.in_dims)
    q_value = self.sess.run(self.pred_network.policy if self.dual_head else self.pred_network.outputs, feed_dict={self.s: state})[0]

    return np.argmax(q_value)

  def update_target_network(self):
    """ If the DQN is not using a dual memory system, the target network is
    updated by copying the predictor network. """
    if not self.wm and not self.dual_head:
      self.target_network.run_copy()

  def observe(self, a, o, r, d, test=False):
    """ Updates the agent's history, and if training, the experience replay and
    networks based on inputted parameters. Values are also passed to Stats for
    recording.

    Parameters
      a: Action most recently taken.
      o: Observed state from taking the action.
      r: Reward received from taking the action.
      d: Whether the environment was terminal after taking the action (True) or
         not (False).
      test: Whether the action was taken in the testing environment (True) or the
            training environment (False).
    """
    if test:
      self.history_test.add(o)
      return

    self.history.add(o)

    r_clip = r
    if r_clip > 0:
      r_clip = 1
    elif r_clip < 0:
      r_clip = -1
    self.experience.add(o, r_clip, a, d)

    if self.t > self.t_learn_start and self.t % self.update_hz == 0:
      q_l_etc = self.update()
      u = True
    else:
      q_l_etc, u = None, False

    if self.t > self.t_learn_start and self.t % self.update_target_hz == 0:
      self.update_target_network()

    self.stats.record(self.t, r, d, np.array(q_l_etc), u, self.summary_writer)

  def update(self):
    """ Updates the predictor network.

    Returns
      List containing the batch's mean Q-value and loss.
    """
    state_batch, action_tmp, reward_batch, next_state_batch, done_batch = self.experience.sample()

    if self.wm:
      feed_dict = {
        self.s: state_batch,
        self.s_: state_batch
      }
    else:
      feed_dict = {
        self.s: state_batch,
        self.s_: next_state_batch,
        self.terminals: done_batch,
        self.rewards: reward_batch,
        self.actions: action_tmp
      }

    if self.dual_head:
      q_t, policy_loss, value_loss, loss, _ = self.sess.run([self.pred_network.value, self.policy_loss, self.value_loss, self.loss, self.optim], feed_dict=feed_dict)
      return np.mean(q_t), policy_loss, value_loss, loss
    q_t, loss, _ = self.sess.run([self.pred_network.outputs, self.loss, self.optim], feed_dict=feed_dict)
    return np.mean(q_t), loss
