# repr_public

This repository contains the code used in [Pseudo-Rehearsal: Achieving Deep Reinforcement Learning without Catastrophic Forgetting](https://arxiv.org/abs/1812.02464).

## License:
- Please cite our [paper](https://arxiv.org/abs/1812.02464) when using this code.
- Our code cannot be used commercially.

## Requirements ##

The details of the conda environment I use for testing can be found in the [file](environment.yml).

## How to run ##

### Learning tasks in short-term memory ###
```$ python main.py --env_name=RoadRunner --save_dir=stm_road0/ --seed=0```

```$ python main.py --env_name=Boxing --save_dir=stm_boxing0/ --seed=0```

```$ python main.py --env_name=Jamesbond --save_dir=stm_jamesbond0/ --seed=0```

### Learning GANs in long-term memory ###
```$ python gan_main.py --exp_load_dir=stm_road0/ --save_dir=gan_road0/ --seed=0```

```$ python gan_main.py --pr=True --exp_load_dir=stm_boxing0/ --gan_load_dir=repr_road_boxing0/ --save_dir=gan_road_boxing0/ --seed=0```

```$ python gan_main.py --pr=True --ratio=0.33 --exp_load_dir=stm_jamesbond0/ --gan_load_dir=repr_road_boxing_jamesbond0/ --save_dir=gan_road_boxing_jamesbond0/ --seed=0```

### Learning tasks in long-term memory ###

#### Transferring the first task to long-term memory ####
In most conditions the first short-term agent is directly transferred to the long-term system, and thus the user does not need to run anything. The one exception is when you want the long-term system to know only a representation of the policy that the short-term system learnt (without the value function). In this case, run:

```$ python main.py --env_name=RoadRunner --wm=True --policy_only=True --new_load_dir=stm_road0/ --save_dir=ltm_policy_road0/ --seed=0```

#### RePR ####
```$ python main.py config/atari_pr.json --env_name=Boxing --prev_env_name="['RoadRunner']" --load_dir=stm_road0/ --new_load_dir=stm_boxing0/ --gan_load_dir=gan_road0/ --save_dir=repr_road_boxing0/ --seed=0```

```$ python main.py config/atari_pr.json --env_name=Jamesbond --prev_env_name="['RoadRunner','Boxing']" --load_dir=repr_road_boxing0/ --new_load_dir=stm_jamesbond0/ --gan_load_dir=gan_road_boxing0/ --save_dir=repr_road_boxing_jamesbond0/ --seed=0```

#### Rehearsal ####
```$ python main.py config/atari_reh.json --env_name=Boxing --prev_env_name="['RoadRunner']" --load_dir=stm_road0/ --new_load_dir=stm_boxing0/ --save_dir=reh_road_boxing0/ --seed=0```

```$ python main.py config/atari_reh.json --env_name=Jamesbond --prev_env_name="['RoadRunner','Boxing']" --prev_load_dir="['stm_road0/']" --load_dir=reh_road_boxing0/ --new_load_dir=stm_jamesbond0/ --save_dir=reh_road_boxing_jamesbond0/ --seed=0```

#### No rehearsal ####
```$ python main.py config/atari_cf.json --env_name=Boxing --prev_env_name="['RoadRunner']" --load_dir=stm_road0/ --new_load_dir=stm_boxing0/ --save_dir=no_reh_road_boxing0/ --seed=0```

```$ python main.py config/atari_cf.json --env_name=Jamesbond --prev_env_name="['RoadRunner', 'Boxing']" --load_dir=no_reh_road_boxing0/ --new_load_dir=stm_jamesbond0/ --save_dir=no_reh_road_boxing_jamesbond0/ --seed=0```

#### EWC ####
```$ rm fisher_0.pckl.gz```

```$ rm fisher_1.pckl.gz```

```$ python main.py config/atari_ewc.json --env_name=Boxing --prev_env_name="['RoadRunner']" --load_dir=stm_road0/ --new_load_dir=stm_boxing0/ --save_dir=ewc_road_boxing0/ --seed=0```

```$ python main.py config/atari_ewc.json --env_name=Jamesbond --prev_env_name="['RoadRunner','Boxing']" --prev_load_dir="['stm_road0/']" --load_dir=ewc_road_boxing0/ --new_load_dir=stm_jamesbond0/ --save_dir=ewc_road_boxing_jamesbond0/ --seed=0```

#### Online-EWC ####
```$ rm fisher_0.pckl.gz```

```$ rm fisher_1.pckl.gz```

```$ python main.py config/atari_ewc.json --env_name=Boxing --prev_env_name="['RoadRunner']" --load_dir=stm_road0/ --new_load_dir=stm_boxing0/ --save_dir=oewc_road_boxing0/ --online_ewc=True --ewc_lambda=75 --seed=0```

```$ python main.py config/atari_ewc.json --env_name=Jamesbond --prev_env_name="['RoadRunner','Boxing']" --prev_load_dir="['stm_road0/']" --load_dir=oewc_road_boxing0/ --new_load_dir=stm_jamesbond0/ --save_dir=oewc_road_boxing_jamesbond0/ --online_ewc=True --ewc_lambda=75 --seed=0```

### Pseudo-rehearsal (without dual memory system) ###
```$ python main.py config/atari_pr.json --env_name=Boxing --prev_env_name="['RoadRunner']" --load_dir=stm_road0/ --wm=False --gan_load_dir=gan_road0/ --save_dir=pr_road_boxing0/ --seed=0```

```$ python main.py config/atari_pr.json --env_name=Jamesbond --prev_env_name="['RoadRunner','Boxing']" --load_dir=pr_road_boxing0/ --wm=False --gan_load_dir=gan_road_boxing0/ --save_dir=pr_road_boxing_jamesbond0/ --seed=0```

### Calculating the Fisher Overlap score ###
```$ python main.py config/atari_F_overlap.json --env_name=Jamesbond --prev_env_name="['RoadRunner','Boxing']" --exp_dir=stm_jamesbond0/ --prev_exp_dir=stm_boxing0/ --prev_prev_exp_dir=stm_road0/ --load_dir=repr_road_boxing_jamesbond0/ --save_dir=repr_F_overlap_road_boxing_jamesbond0/ --seed=0```

## Running notes ##
- A list of the experimental condition's default hyper-parameters can be found in their corresponding [config/](config/) file.
- For simplicity, run all short-term learning before starting long-term learning (only the first line is needed in the case of pseudo-rehearsal without the dual memory system).
- When running either RePR or pseudo-rehearsal (without dual memory system), alternate between training the GAN and training the agent.
- The Fisher Overlap can be calculated for other networks by specifying their ```load_dir```.
- The implementation of the GAN currently does pseudo-rehearsal with sequences drawn from the previous GAN as stored and used by the current long-term agent. In future implementations this should change so that the previous GAN generates its own sequences right before learning the new GAN.
- For the (online-)EWC conditions it is expected that you should have to confirm running even though the weights are not equal ONCE (for when it loads the Fisher matrix for the first task) when training on all three tasks ONLY.
