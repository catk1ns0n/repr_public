import tensorflow as tf

def reduce_var(x, axis=None, keepdims=False):
  """Variance of a tensor, alongside the specified axis.

  Sourced from:
    https://stackoverflow.com/questions/39354566/what-is-the-equivalent-of-np-std-in-tensorflow

  Parameters
    x: A tensor or variable.
    axis: An integer, the axis to compute the variance.
    keepdims: A boolean, whether to keep the dimensions or not.
              If 'keepdims' is 'False', the rank of the tensor is reduced
              by 1. If 'keepdims' is 'True',
              the reduced dimension is retained with length 1.

  Returns
    A tensor with the variance of elements of 'x'.
  """
  m = tf.reduce_mean(x, axis=axis, keep_dims=True)
  devs_squared = tf.square(x - m)
  return tf.reduce_mean(devs_squared, axis=axis, keep_dims=keepdims)

def reduce_std(x, axis=None, keepdims=False):
  """Standard deviation of a tensor, alongside the specified axis.

  Sourced from:
    https://stackoverflow.com/questions/39354566/what-is-the-equivalent-of-np-std-in-tensorflow

  Parameters
    x: A tensor or variable.
    axis: An integer, the axis to compute the standard deviation.
    keepdims: A boolean, whether to keep the dimensions or not.
              If 'keepdims' is 'False', the rank of the tensor is reduced
              by 1. If 'keepdims' is 'True',
              the reduced dimension is retained with length 1.

  Returns
    A tensor with the standard deviation of elements of 'x'.
  """
  return tf.sqrt(reduce_var(x, axis=axis, keepdims=keepdims))

def linear(var_scope, input_, out_num,
  weights_initializer=tf.contrib.layers.xavier_initializer(),
  biases_initializer=tf.constant_initializer(0.1), trainable=True):
  """ Applies a linear layer to the input, returning the resulting
  layer and weights.

  Parameters
    var_scope: Variable scope of layer.
    input_: Input to the layer.
    out_num: Number of output units in the layer.
    weights_initializer: Initialiser to use for weights.
    biases_initializer: Initialiser to use for biases.
    trainable: Whether the layer's weights are trainable (True) or not (False).

  Returns
    Tuple: (layer's output, (weights, biases)).
  """
  with tf.variable_scope(var_scope):
    in_num = input_.get_shape().as_list()[1]
    W = tf.get_variable("W", (in_num, out_num), tf.float32, trainable=trainable,
      initializer=weights_initializer)
    b = tf.get_variable("b", (out_num,), tf.float32, trainable=trainable,
      initializer=biases_initializer)
    layer_out = tf.nn.bias_add(tf.matmul(input_, W), b)
    return layer_out, [W, b]

def conv2d(var_scope, input_, k_num, k_h=3, k_w=3, s_h=1, s_w=1,
  weights_initializer=tf.contrib.layers.xavier_initializer(),
  biases_initializer=tf.constant_initializer(0.1), trainable=True, data_format='NHWC',
  padding='VALID'):
  """ Applies a 2D convolutional layer to the input, returning the resulting
  layer and weights.

  Parameters
    var_scope: Variable scope of layer.
    input_: Input to the layer.
    k_num: Number of kernels/filters to apply.
    k_h: Kernel/filter height.
    k_w: Kernel/filter width.
    s_h: Kernel/filter stride (height).
    s_w: Kernel/filter stride (width).
    weights_initializer: Initialiser to use for weights.
    biases_initializer: Initialiser to use for biases.
    trainable: Whether the layer's weights are trainable (True) or not (False).
    data_format: Format of input images. Valid values are: ['NHWC', 'NCHW'].
    padding: Whether to use padding or not. Valid values are ['VALID', 'SAME'].

  Returns
    Tuple: (layer's output, (weights, biases)).
  """
  with tf.variable_scope(var_scope):
    c_num = input_.get_shape().as_list()[-1]
    stride = [1, s_h, s_w, 1]
    if data_format == 'NCHW':
      c_num = input_.get_shape().as_list()[1]
      stride = [1, 1, s_h, s_w]
    W_shape = (k_h, k_w, c_num, k_num)
    W = tf.get_variable("W", W_shape, tf.float32, trainable=trainable,
      initializer=weights_initializer)
    b = tf.get_variable("b", (k_num,), tf.float32, trainable=trainable,
      initializer=biases_initializer)
    layer_out = tf.nn.bias_add(tf.nn.conv2d(input_, W, stride, padding=padding, data_format=data_format), b, data_format=data_format)
    return layer_out, [W, b]

def deconv2d(var_scope, input_, k_num, k_h=3, k_w=3, s_h=1, s_w=1,
  weights_initializer=tf.contrib.layers.xavier_initializer(),
  biases_initializer=tf.constant_initializer(0.1), trainable=True, data_format='NHWC',
  padding='SAME'):
  """ Applies a 2D deconvolutional layer to the input, returning the resulting
  layer and weights.

  Parameters
    var_scope: Variable scope of layer.
    input_: Input to the layer.
    k_num: Number of kernels/filters to apply.
    k_h: Kernel/filter height.
    k_w: Kernel/filter width.
    s_h: Kernel/filter stride (height).
    s_w: Kernel/filter stride (width).
    weights_initializer: Initialiser to use for weights.
    biases_initializer: Initialiser to use for biases.
    trainable: Whether the layer's weights should be trainable (True) or not (False).
    data_format: Format of input images. Valid values are: ['NHWC', 'NCHW'].
    padding: Whether to use padding or not. Valid values are ['VALID', 'SAME'].

  Returns
    Tuple: (layer's output, (weights, biases)).
  """
  with tf.variable_scope(var_scope):
    in_dims = input_.get_shape().as_list()
    stride = [1, s_h, s_w, 1]
    batch_size, h, w, c_num = in_dims
    output_shape = [batch_size, s_h*h, s_w*w, k_num]
    if data_format == 'NCHW':
      stride = [1, 1, s_h, s_w]
      batch_size, c_num, h, w = in_dims
      output_shape = [batch_size, k_num, s_h*h, s_w*w]
    W_shape = (k_h, k_w, k_num, c_num)
    W = tf.get_variable("W", W_shape, tf.float32, trainable=trainable,
      initializer=weights_initializer)
    b = tf.get_variable("b", (k_num,), tf.float32, trainable=trainable,
      initializer=biases_initializer)
    layer_out = tf.nn.bias_add(tf.nn.conv2d_transpose(input_, W, output_shape, stride,
      padding=padding, data_format=data_format), b, data_format=data_format)
    return layer_out, [W, b]

def maxpool(var_scope, input_, k_h=3, k_w=3, s_h=2, s_w=2, data_format='NHWC', padding='VALID'):
  """ Applies a max-pooling layer to the input, returning the resulting
  layer.

  Parameters
    var_scope: Variable scope of layer.
    k_h: Kernel height.
    k_w: Kernel width.
    s_h: Kernel stride (height).
    s_w: Kernel stride (width).
    data_format: Format of input images. Valid values are: ['NHWC', 'NCHW'].
    padding: Whether to use padding or not. Valid values are ['VALID', 'SAME'].

  Returns
    Layer's output.
  """
  with tf.variable_scope(var_scope):
    k_shape = [1, k_h, k_w, 1]
    stride = [1, s_h, s_w, 1]
    if data_format == 'NCHW':
      k_shape = [1, 1, k_h, k_w]
      stride = [1, 1, s_h, s_w]
    return tf.nn.max_pool(input_, ksize=k_shape, strides=stride, padding=padding, data_format=data_format)